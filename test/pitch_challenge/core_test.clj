(ns pitch-challenge.core-test
  (:require [clojure.test :refer :all]
            [pitch-challenge.core :refer :all]))

(deftest valid-numbers
  (testing "123456789"
    (let [entry-numbers (str "    _  _     _  _  _  _  _ " "\n"
                             "  | _| _||_||_ |_   ||_||_|" "\n"
                             "  ||_  _|  | _||_|  ||_| _|")
          account-numbers (parse-input entry-numbers)]
      (is (= "123456789" account-numbers))))

  (testing "000000000"
    (let [entry-numbers (str " _  _  _  _  _  _  _  _  _ " "\n"
                             "| || || || || || || || || |" "\n"
                             "|_||_||_||_||_||_||_||_||_|")
          account-numbers (parse-input entry-numbers)]
      (is (= "000000000" account-numbers))))

  (testing "490067115"
    (let [entry-numbers (str "    _  _  _  _  _        _ " "\n"
                             "|_||_|| || ||_   |  |  ||_ " "\n"
                             "  | _||_||_||_|  |  |  | _|")
          account-numbers (parse-input entry-numbers)]
      (is (= "490067115" account-numbers)))))


(deftest invalid-checksum
  (testing "111111111"
    (let [entry-numbers (str "                           " "\n"
                             "  |  |  |  |  |  |  |  |  |" "\n"
                             "  |  |  |  |  |  |  |  |  |")
          account-numbers (parse-input entry-numbers)]
      (is (= "111111111 ERR" account-numbers))))

  (testing "222222222"
    (let [entry-numbers (str " _  _  _  _  _  _  _  _  _ " "\n"
                             " _| _| _| _| _| _| _| _| _|" "\n"
                             "|_ |_ |_ |_ |_ |_ |_ |_ |_ ")
          account-numbers (parse-input entry-numbers)]
      (is (= "222222222 ERR" account-numbers))))

  (testing "333333333"
    (let [entry-numbers (str " _  _  _  _  _  _  _  _  _ " "\n"
                             " _| _| _| _| _| _| _| _| _|" "\n"
                             " _| _| _| _| _| _| _| _| _|")
          account-numbers (parse-input entry-numbers)]
      (is (= "333333333 ERR" account-numbers))))

  (testing "444444444"
    (let [entry-numbers (str "                           " "\n"
                             "|_||_||_||_||_||_||_||_||_|" "\n"
                             "  |  |  |  |  |  |  |  |  |")
          account-numbers (parse-input entry-numbers)]
      (is (= "444444444 ERR" account-numbers))))

  (testing "555555555"
    (let [entry-numbers (str " _  _  _  _  _  _  _  _  _ " "\n"
                             "|_ |_ |_ |_ |_ |_ |_ |_ |_ " "\n"
                             " _| _| _| _| _| _| _| _| _|")
          account-numbers (parse-input entry-numbers)]
      (is (= "555555555 ERR" account-numbers))))

  (testing "666666666"
    (let [entry-numbers (str " _  _  _  _  _  _  _  _  _ " "\n"
                             "|_ |_ |_ |_ |_ |_ |_ |_ |_ " "\n"
                             "|_||_||_||_||_||_||_||_||_|")
          account-numbers (parse-input entry-numbers)]
      (is (= "666666666 ERR" account-numbers))))

  (testing "777777777"
    (let [entry-numbers (str " _  _  _  _  _  _  _  _  _ " "\n"
                             "  |  |  |  |  |  |  |  |  |" "\n"
                             "  |  |  |  |  |  |  |  |  |")
          account-numbers (parse-input entry-numbers)]
      (is (= "777777777 ERR" account-numbers))))

  (testing "888888888"
    (let [entry-numbers (str " _  _  _  _  _  _  _  _  _ " "\n"
                             "|_||_||_||_||_||_||_||_||_|" "\n"
                             "|_||_||_||_||_||_||_||_||_|")
          account-numbers (parse-input entry-numbers)]
      (is (= "888888888 ERR" account-numbers))))

  (testing "999999999"
    (let [entry-numbers (str " _  _  _  _  _  _  _  _  _ " "\n"
                             "|_||_||_||_||_||_||_||_||_|" "\n"
                             " _| _| _| _| _| _| _| _| _|")
          account-numbers (parse-input entry-numbers)]
      (is (= "999999999 ERR" account-numbers)))))


(deftest illegible-characters
  (testing "1??1?1?11"
    (let [entry-numbers (str "     |  |     |     |      " "\n"
                             "  |  |  |  |  |  |  |  |  |" "\n"
                             "  |  |  |  |  |  |  |  |  |")
          account-numbers (parse-input entry-numbers)]
      (is (= "1??1?1?11 ILL" account-numbers))))

  (testing "12345678?"
    (let [entry-numbers (str "    _  _     _  _  _  _  _ " "\n"
                             "  | _| _||_||_ |_   ||_||_|" "\n"
                             "  ||_  _|  | _||_|  ||_| _ ")
          account-numbers (parse-input entry-numbers)]
      (is (= "12345678? ILL" account-numbers))))

  (testing "49006771?"
    (let [entry-numbers (str "    _  _  _  _  _  _     _ " "\n"
                             "|_||_|| || ||_   |  |  | _ " "\n"
                             "  | _||_||_||_|  |  |  | _|")
          account-numbers (parse-input entry-numbers)]
      (is (= "49006771? ILL" account-numbers)))))
