(ns pitch-challenge.core
  (:require [clojure.string :as cstr])
  (:gen-class))

;; 3x3 digits numbers mapping
(def VALUE_NUMBERS
  {(str " _ "
        "| |"
        "|_|") 0

   (str "   "
        "  |"
        "  |") 1

   (str " _ "
        " _|"
        "|_ ") 2

   (str " _ "
        " _|"
        " _|") 3

   (str "   "
        "|_|"
        "  |") 4

   (str " _ "
        "|_ "
        " _|") 5

   (str " _ "
        "|_ "
        "|_|") 6

   (str " _ "
        "  |"
        "  |") 7

   (str " _ "
        "|_|"
        "|_|") 8

   (str " _ "
        "|_|"
        " _|") 9})

(defn list->str [list]
  (apply str list))

(defn valid-digits?
  "Check if every element in list is a number"
  [digits-list]
  (every? number? digits-list))

(defn entries-count
  "Get entries count, by counting a length of the first row"
  [rows]
  (-> rows first count))

(defn get-rows
  "Split the input string into list of 3 rows"
  [input]
  (->> input
       cstr/split-lines
       (filter not-empty)))

(defn get-cols
  "Transform a row into list which contains 9 strings with 3 characters each"
  [row]
  (->> row
       (partition 3)
       (map list->str)))

(defn symbol->digit
  "Return a single digit by getting a value from the 'VALUE_NUMBERS'
   If a digit isn't found - return '?'"
  [rows i]
  (let [get-value-numbers #(get VALUE_NUMBERS % "?")]
    (->> rows
         (map #(nth % i))
         list->str
         get-value-numbers)))

(defn get-digits-list
  "Return a list of digits"
  [rows]
  (->> rows
       entries-count
       range
       (map (partial symbol->digit rows))))

(defn calc-checksum
  "Return 'nil' if the 'digits-list' contains not numeric characters
   Return the checksum calculated by (d1+2*d2+3*d3 +..+9*d9) mod 11"
  [digits-list]
  (when (valid-digits? digits-list)
    (let [apply-mod #(mod % 11)]
      (->> digits-list
           reverse
           (map-indexed (fn [i digit]
                          (* digit (inc i))))
           (apply +)
           apply-mod))))

(defn validate
  "Return the 'digits' with 'ILL' flag when some characters are illegible
   Return the 'digits' with 'ERR' flag when checksum is wrong
   Return the 'digits' if all checks are passed"
  [[digits checksum]]
  (cond
    (nil? checksum) (str digits " ILL")
    (not (zero? checksum)) (str digits " ERR")
    :else digits))

(defn parse-input
  "Parse the input string and return string with all account numbers"
  [input]
  (->> input
       get-rows
       (partition 3)
       (map #(let [digits-list (->> %
                                    (map get-cols)
                                    get-digits-list)
                   checksum (calc-checksum digits-list)
                   digits (list->str digits-list)]
               [digits checksum]))
       (map validate)
       (cstr/join "\n")))

(defn -main
  "Application entry point"
  [& args]
  (let [[input output] args]
    (->> input
         slurp
         parse-input
         (spit output))))
